﻿using UnityEngine;
using System.Collections;

public class MapPointType : MonoBehaviour
{

	public enum Types
	{
		Shelter,
		Monster,
		Beacon

	}
	public Types type;

	public Sprite monsterSpriteActive, monsterSpriteInactive, shelterSpriteActive, shelterSpriteInactive;
	public Sprite[] beaconSprites;

	SpriteRenderer renderer;

	void Awake()
	{
		renderer = GetComponentInChildren<SpriteRenderer>();

	}

	public void SetSprite(Types type, int active)
	{
		this.type = type;
		if (type == Types.Beacon) {
			// set whichever beacon sprite fits
			renderer.sprite = beaconSprites[Mathf.Clamp(active, 0, beaconSprites.Length)];

		} else {

			switch (type) {
			case Types.Shelter:
				renderer.sprite = active > 0 ? shelterSpriteActive : shelterSpriteInactive;
				break;
			case Types.Monster:
				renderer.sprite = active > 0 ? monsterSpriteActive : monsterSpriteInactive;
				break;
			default:
				break;
			}

		}
	}

}
