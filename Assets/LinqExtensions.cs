﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

namespace System.Linq
{
	public static class LinqExtensions
	{
		/// <summary>
		/// Returns the transform from the list which is closest to the position specified. uses sqrMagnitude and Aggregate(). returns null if not found.
		/// </summary>
		/// <param name="transformList">list of transforms to choose from</param>
		/// <param name="position">position to compare distance with</param>
		/// <returns>the closest transform to the position, from the list</returns>
		public static Transform ClosestTransform(this IEnumerable<Transform> transformList, Vector3 position)
		{
			if (transformList.Any()) {
				return transformList.Aggregate((t1, t2) => (t1.position - position).sqrMagnitude > (t2.position - position).sqrMagnitude ? t2 : t1);
			} else {
				return null;
			}
		}

		/// <summary>
		/// Returns the collider from the list which is closest to the position specified. uses sqrMagnitude and Aggregate(). returns null if not found.
		/// </summary>
		/// <param name="colliderList">list to choose from</param>
		/// <param name="position"></param>
		/// <returns></returns>
		public static Collider ClosestCollider(this IEnumerable<Collider> colliderList, Vector3 position)
		{
			if (colliderList.Any()) {
				return colliderList.Aggregate((t1, t2) => (t1.transform.position - position).sqrMagnitude > (t2.transform.position - position).sqrMagnitude ? t2 : t1);
			} else {
				return null;
			}
		}

	}
}