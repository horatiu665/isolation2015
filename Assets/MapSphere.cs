﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class MapSphere : MonoBehaviour
{

	List<MapSphere> otherSafeHouses = new List<MapSphere>();
	List<Transform> evilMonsters = new List<Transform>();
	List<Transform> mapPoints = new List<Transform>();
	public Transform mapPointPrefab;
	public AnimationCurve scaleBasedOnDistance;

	public Collider mapOnThisCollider;

	public float distanceFromCenter = 7f;
	public float yOffset = 1f;

	public Transform player;

	// Use this for initialization
	IEnumerator Start()
	{
		yield return new WaitForSeconds(1F);
		
	}

	// not a good idea
	//IEnumerator ResetMapConstantly()
	//{
	//	while (true) {
	//		ResetMap();
	//		yield return new WaitForSeconds(3);
	//	}
	//}

	void OnTriggerEnter(Collider c)
	{
		if (c.GetComponent<PlayerSafe>()) {
			if (player == null) {
				player = c.transform;
			}
			ShowMapInstant();
		}

	}

	void OnTriggerExit(Collider c)
	{
		if (c.GetComponent<PlayerSafe>()) {
			HideMapInstant();
		}
	}

	void ResetLists()
	{

		otherSafeHouses = GameObject.FindObjectsOfType<MapSphere>().Where(ms => ms != this).ToList();
		evilMonsters = GameObject.FindGameObjectsWithTag("EvilMonster").Select(go => go.transform).ToList();

	}

	// reset map only if it is already active. used by other objects to update map with new enemies or some shit
	public void ResetMap()
	{
		if (mapPoints.Count > 0) {
			ShowMapInstant();
		}
	}

	public void ShowMapInstant()
	{
		HideMapInstant();
		
		ResetLists();

		foreach (var sh in otherSafeHouses) {
			// create dot on the map, from center 
			//sh.transform.position;
			//transform.position;
			var pos = sh.transform.position - player.position;
			var distance = pos.magnitude;
			pos = pos / distance * distanceFromCenter;
			pos += player.position + Vector3.up * yOffset;

			var a = Instantiate(mapPointPrefab, pos, Quaternion.identity) as Transform;
			var shelterIsActive = sh.GetComponentInParent<ShelterActivation>().IsActive;
			a.GetComponent<MapPointType>().SetSprite(MapPointType.Types.Shelter, shelterIsActive ? 1 : 0);
			a.localScale = Vector3.one * scaleBasedOnDistance.Evaluate(distance);
			mapPoints.Add(a);
		}

		foreach (var m in evilMonsters) {
			var pos = m.transform.position - player.position;
			var distance = pos.magnitude;
			pos = pos / distance * distanceFromCenter;
			pos += player.position + Vector3.up * yOffset;

			var a = Instantiate(mapPointPrefab, pos, Quaternion.identity) as Transform;
			a.GetComponent<MapPointType>().SetSprite(MapPointType.Types.Monster, 1);
			a.localScale = Vector3.one * scaleBasedOnDistance.Evaluate(distance);
			mapPoints.Add(a);
		}

		// set beacon sprite and count shelters.
	}

	public float offset = 1f;

	void Update()
	{
		// update positions and scales of the map points based on shelters
		if (player != null) {
			int i = 0;
			foreach (var sh in otherSafeHouses) {
				if (mapPoints.Count > i) {
					//var distance = (sh.transform.position - player.position).magnitude;
					//mapPoints[i].position = player.position + (sh.transform.position - player.position) / distance * distanceFromCenter;

					var playerPos = player.position;
					var targetPos = sh.transform.position;
					var spherePos = transform.position;
					var mapPointPos = Vector3.zero;

					//Debug.DrawRay(playerPos, Vector3.up, Color.green);
					//Debug.DrawRay(targetPos, Vector3.up, Color.blue);
					//Debug.DrawRay(spherePos, Vector3.up, Color.cyan);
					//var cc = Vector3.Angle(targetPos - spherePos, targetPos - playerPos);
					//var heightSize = (spherePos - targetPos).magnitude * Mathf.Sin(cc);
					//print(heightSize);

					//return;
					//var sphereToTarget = (spherePos - targetPos).magnitude;

					//var x = Mathf.Sin(Mathf.Acos(sphereToTarget * Mathf.Sin(cc)));

					//var mapPointPos = (targetPos - playerPos).normalized * x;

					// default value
					mapPoints[i].position = Vector3.up * -10000;

					var allHits = Physics.RaycastAll(targetPos, playerPos - targetPos);
					if (allHits.Count() > 0) {
						//var thisHit = allHits.First(rh => rh.collider == GetComponent<SphereCollider>());
						RaycastHit thisHit = allHits[0];
						bool found = false;
						for (int j = 0; j < allHits.Length; j++) {
							if (allHits[j].collider == mapOnThisCollider) {
								thisHit = allHits[j];
								found = true;
							}
						}
						if (found) {
							
							mapPointPos = thisHit.point + (thisHit.point - playerPos).normalized * offset;
							
							mapPoints[i].position = mapPointPos;

							bool shelterIsActive = sh.GetComponentInParent<ShelterActivation>().IsActive;
							mapPoints[i].GetComponent<MapPointType>().SetSprite(MapPointType.Types.Shelter, shelterIsActive ? 1 : 0);
							mapPoints[i].localScale = Vector3.one * scaleBasedOnDistance.Evaluate((targetPos - playerPos).magnitude);
			

						}
					}


					i++;
				}
			}

			foreach (var m in evilMonsters) {
				if (mapPoints.Count > i) {
					
					if (m==null || m.transform == null) {
						ResetMap();
						return;
					}
					var playerPos = player.position;
					var targetPos = m.transform.position;
					var spherePos = transform.position;
					var mapPointPos = Vector3.zero;

					// default value
					mapPoints[i].position = Vector3.up * -10000;

					var allHits = Physics.RaycastAll(targetPos, playerPos - targetPos);
					if (allHits.Count() > 0) {
						//var thisHit = allHits.First(rh => rh.collider == GetComponent<SphereCollider>());
						RaycastHit thisHit = allHits[0];
						bool found = false;
						for (int j = 0; j < allHits.Length; j++) {
							if (allHits[j].collider == mapOnThisCollider) {
								thisHit = allHits[j];
								found = true;
							}
						}
						if (found) {
							mapPointPos = thisHit.point + (thisHit.point - playerPos).normalized * offset;
							
							mapPoints[i].position = mapPointPos;

							bool shelterIsActive = true;
							mapPoints[i].GetComponent<MapPointType>().SetSprite(MapPointType.Types.Monster, shelterIsActive ? 1 : 0);
							mapPoints[i].localScale = Vector3.one * scaleBasedOnDistance.Evaluate((targetPos - playerPos).magnitude);
						}
					}

					i++;
				}
			}

		}

	}

	void HideMapInstant()
	{
		foreach (var mp in mapPoints) {
			Destroy(mp.gameObject);
		}

		mapPoints.Clear();

	}




}
