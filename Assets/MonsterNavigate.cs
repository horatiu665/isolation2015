﻿using UnityEngine;
using System.Collections;

public class MonsterNavigate : MonoBehaviour
{
	public float height = -0.2f;
	public Transform target;
	public float movementSpeed;

	void Update()
	{
		MoveTo(target.position);

	}


	void MoveTo(Vector3 target)
	{
		PositionOnObject.PositionOn(transform, null, Vector3.down, 10f, false, Vector3.zero, false, height, null);
		var lookAtTarget = target;
		lookAtTarget.y = transform.position.y;

		transform.LookAt(lookAtTarget);
		var dir = target - transform.position;
		dir.Normalize();
		transform.Translate(dir * movementSpeed * Time.deltaTime);

	}

}
