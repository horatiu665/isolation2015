﻿using UnityEngine;
using System.Collections;
using System.Linq;


[RequireComponent(typeof(RotationModule))]
public class StateIdleFly : MonoBehaviour, IState
{
	public Vector3 speed;
	BirdFunc bird;
	private RotationModule rotationModule;
	public Transform[] targets;

	public AnimationCurve resetRotationTimerDistribution;
	float rotationTimerReset;

	public float basePriority;

	IEnumerator Start()
	{
		bird = GetComponent<BirdFunc>();
		rotationModule = GetComponent<RotationModule>();

		yield return 0;
		yield return 0;

		// set targets to be shelters
		targets = GameObject.FindObjectsOfType<ShelterActivation>().Select(sa => sa.transform).ToArray();

	}

	public string GetName()
	{
		return "Idle fly";
	}

	public bool GetUninterruptible()
	{
		return false;
	}

	public float GetPriority()
	{
		return basePriority;
	}

	public void OnEnter()
	{
		ChooseRotationTowardsTarget();
	}

	void ChooseRotationTowardsTarget()
	{
		if (targets.Length == 0) return;
		Transform target;
		// choose active shelters, and if there are none, choose inactive shelters which do not have any dudes around
		var activeShelters = targets.Where(t => t.GetComponent<ShelterActivation>().IsActive);
		if (activeShelters.Any()) {
			// go to closest active shelter
			target = activeShelters.ClosestTransform(transform.position);
		} else {
			// go to inactive shelter where there is no dude around
			target = targets.ClosestTransform(transform.position);

		}

		if (target == null) return;

		// rotate towards wherever is closer to target
		if ((transform.position + transform.forward + transform.right - target.position).sqrMagnitude > (transform.position + transform.forward - transform.right - target.position).sqrMagnitude) {
			speed.x = -Mathf.Abs(speed.x);
		} else {
			speed.x = Mathf.Abs(speed.x);
		}
		//speed.x = speed.x * (Random.Range(0, 1) * 2 - 1);

		rotationTimerReset = Time.time + resetRotationTimerDistribution.Evaluate(Random.Range(0, 1f));
	}

	public void OnExecute(float deltaTime)
	{
		if (rotationTimerReset < Time.time) {
			ChooseRotationTowardsTarget();
		}

		var flyDir = transform.forward * speed.z + transform.right * speed.x + transform.up * speed.y;

		var lookdir = flyDir;
		lookdir.y = 0;

		rotationModule.RotateTowards(lookdir);

		// fly in the sky
		transform.position += flyDir * deltaTime;

		bird.FlyDown(deltaTime);
	}

	public void OnExit()
	{

	}

	/// <summary>
	/// idle is always an option
	/// </summary>
	/// <returns></returns>
	public bool ConditionsMet()
	{
		return true;
	}
}
