﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

	public class ControllerState : MonoBehaviour, IState
	{
		public List<IState> states = new List<IState>();
		public IState currentState;

		bool InitStates()
		{
			// sets initial state, etc
			var validStates = states.Where(s => s.ConditionsMet());
			if (validStates.Count() > 0)
				currentState = validStates.Aggregate((s1, s2) =>
					s1.GetPriority() < s2.GetPriority() ? s2 : s1);
			if (currentState == null)
				return false;
			currentState.OnEnter();
			return true;
		}

		void UpdateStates(float deltaTime)
		{
			if (currentState == null) {
				if (!InitStates()) {
					return;
				}
			}

			if (!currentState.GetUninterruptible()) {
				// takes list of valid states, chooses highest distTargetPriority. 
				var validStates = states.Where(s => s.ConditionsMet()).ToList();
				if (!validStates.Any()) {
					return;
				}
				IState highestPriorityState = validStates.First();
				float maxPriority = float.MinValue;
				for (int i = 0; i < validStates.Count(); i++) {
					var p = validStates[i].GetPriority();
					if (p > maxPriority) {
						maxPriority = p;
						highestPriorityState = validStates[i];
					}
				}
				//var highestPriorityState = validStates.Aggregate((s1, s2)
				//	=> s1.GetPriority() < s2.GetPriority() ? s2 : s1);

				// if different than current state, switch to it.
				if (highestPriorityState != currentState) {
					currentState.OnExit();
					currentState = highestPriorityState;
					currentState.OnEnter();
				}
			}

			currentState.OnExecute(deltaTime);

		}

		public bool getStatesFromChildren;
		public bool initStates;

		public string name = "Controller State";
		public float priority;

		public string GetName()
		{
			if (currentState != null) {
				return name + " > " + currentState.GetName();
			} else {
				return name;
			}
		}

		public bool GetUninterruptible()
		{
			return currentState.GetUninterruptible();
		}

		public float GetPriority()
		{
			// find the highest distTargetPriority among states. not just current state (because nothing will IsActive ever if only current state is considered
			var maxPriority = states.Max(s => s.ConditionsMet() ? s.GetPriority() : 0);
			return priority + maxPriority;
		}

		public void OnEnter()
		{
			InitStates();
		}

		public void OnExecute(float deltaTime)
		{
			if (runIndependently) {
				UpdateStates(deltaTime);
			} else {
				updateTimer += deltaTime;
				if (updateTimer >= updateTime) {
					updateTimer = 0;
					UpdateStates(Mathf.Max(updateTime, Time.deltaTime));
				}
			}
		}

		public void OnExit()
		{
			currentState.OnExit();
			currentState = null;
		}

		public bool ConditionsMet()
		{
			if (states == null) return false;
			return states.Any(s => s.ConditionsMet());
		}

		// if this is not part of another state machine, it should run based on unity's events
		public bool runIndependently = false;


		// unity messages

		IEnumerator Start()
		{
			if (getStatesFromChildren) {
				foreach (IState s in GetComponentsInChildren<IState>()) {
					if (s != this) {
						states.Add(s);
					}
				}
				if (states.Any(s => s is ControllerState)) {
					yield return 0;
					// remove states that are inside controllerstates
					List<IState> ssss = new List<IState>(states.Where(s => s is ControllerState));
					foreach (ControllerState cs in ssss) {
						if (cs.states != null) {
							foreach (var ss in cs.states) {
								states.Remove(ss);
							}
						}
					}
				}
			}
			if (initStates) {
				InitStates();
			}
			StartCoroutine(UpdateInfreq());

		}
		public float updateTime = 0f;
		float updateTimer = 0f;

		IEnumerator UpdateInfreq()
		{
			var r = Random.Range(0f, updateTime);
			yield return new WaitForSeconds(r);
			while (true) {
				if (runIndependently) {
					UpdateStates(Mathf.Max(updateTime, Time.deltaTime));
				} 
				yield return new WaitForSeconds(updateTime);
			}
		}

		public Gradient color;
		public bool drawStateSphere;
		public Vector2 stateSphereSize;


		void OnDrawGizmos()
		{
			if (color == null) return;
			if (states == null) return;
			if (currentState == null) return;

			if (drawStateSphere) {
				Gizmos.color = color.Evaluate(states.IndexOf(currentState) / (float)(states.Count - 1));
				Gizmos.DrawSphere(transform.position + Vector3.up * stateSphereSize.x, stateSphereSize.y);
			}
		}

	}
