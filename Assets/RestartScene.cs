﻿using UnityEngine;
using System.Collections;

public class RestartScene : MonoBehaviour {

	public static RestartScene instance;

	void Awake()
	{
		instance = this;
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Restart")) {
			RestartLevel();
		}
	}


	public static void RestartLevel()
	{
		Application.LoadLevel(Application.loadedLevel);
	}
}
