﻿using UnityEngine;
using System.Collections;

public class LightningSometimes : MonoBehaviour
{

	public AnimationCurve frequencyDistribution;
	public Transform[] lightnings;

	void Start()
	{
		StartCoroutine(Spawn());
	}

	IEnumerator Spawn()
	{
		while (true) {
			var a = Instantiate(lightnings[Random.Range(0, lightnings.Length)], transform.position, transform.rotation) as Transform;

			yield return new WaitForSeconds(frequencyDistribution.Evaluate(Random.Range(0, 1f)));
		}
	}
}
