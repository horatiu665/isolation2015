﻿using UnityEngine;
using System.Collections;

public class AnimateTexture : MonoBehaviour {

	public float speed = 0.1f;
	Vector2 offset;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		offset.x += speed * Time.deltaTime;
		var mat = GetComponent<Renderer>().materials[0];
		mat.SetTextureOffset("_BumpMap", offset);
		//mat.SetTextureOffset("_MainTex", offset);

	}
}
