﻿using UnityEngine;
using System.Collections;

public class ThunderRandom : MonoBehaviour
{

	public AnimationCurve distribution;
	public AnimationCurve[] intensity;
	public float duration;
	Light light;

	void Start()
	{
		light = GetComponent<Light>();
		Invoke("Thunder", distribution.Evaluate(Random.Range(0, 1f)));
	}


	void Thunder()
	{
		int randThunderShape = Random.Range(0, intensity.Length);
		StartCoroutine(pTween.To(duration, t => {
			light.intensity = intensity[randThunderShape].Evaluate(t);
			
		}));

		// play sound
		//SOUNDSOUNDSOUNDSOUNDSOUNDS

		Invoke("Thunder", distribution.Evaluate(Random.Range(0, 1f)));
	}

}
