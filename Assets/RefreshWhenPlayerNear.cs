﻿using UnityEngine;
using System.Collections;

public class RefreshWhenPlayerNear : MonoBehaviour
{
	public Transform player;
	public float rangeToUpdate = 50f;
	ReflectionProbe brbobe;

	void Start()
	{
		brbobe = GetComponent<ReflectionProbe>();
		StartCoroutine(UpdateSlow());
	}

	IEnumerator UpdateSlow()
	{
		yield return new WaitForSeconds(Random.Range(0, 0.5f));
		while (true) {
			if (player != null) {
				var playerNear = (player.transform.position - transform.position).sqrMagnitude < rangeToUpdate * rangeToUpdate;

				if (playerNear) {
					brbobe.RenderProbe();

				}
			}
			yield return new WaitForSeconds(0.5f);
		}
	}
}
