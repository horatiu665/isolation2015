﻿using UnityEngine;
using System.Collections;

public class RestartableObject : MonoBehaviour {

	Vector3 initPos;
	Quaternion initRot;

	// Use this for initialization
	void Start () {
		initPos = transform.position;
		initRot = transform.rotation;
	}
	

	public void RestartObject()
	{
		transform.position = initPos;
		transform.rotation = initRot;


	}
}
