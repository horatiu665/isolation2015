﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class ShelterActivation : MonoBehaviour
{

	public Light light;
	public AnimationCurve lightIntensity;
	public Gradient lightColor;
	public Transform scaler;
	public AnimationCurve scale;

	public float animDuration;

	public void SetActive(bool active)
	{
		StartCoroutine(pTween.To(animDuration, active ? 0 : 1, active ? 1 : 0, t => {
			SetActive(t);
		}));

	}

	public void SetActive(float active)
	{
		light.color = lightColor.Evaluate(active);
		light.intensity = lightIntensity.Evaluate(active);
		scaler.localScale = Vector3.one * scale.Evaluate(active);

	}

	void Start()
	{
		// set active
		SetActive(IsActive);
		isActive = IsActive;

		StartCoroutine(CheckForGhosts());
		StartCoroutine(CheckForPlayer());
	}

	public bool IsActive;
	bool isActive;

	[Range(0, 1f)]
	public float active;
	float ac;

	public float ghostDeactivationRange = 10f;
	public int minGhostDeactivationCount = 3;


	void Update()
	{
		if (active != ac) {
			ac = active;
			SetActive(active);
		}
		if (IsActive != isActive) {
			isActive = IsActive;
			SetActive(IsActive);
		}
	}


	IEnumerator CheckForGhosts()
	{
		while (true) {
			var ghostsNearby = GameObject.FindGameObjectsWithTag("EvilMonster").Where(go => (go.transform.position - transform.position).sqrMagnitude < ghostDeactivationRange * ghostDeactivationRange);
			if (ghostsNearby.Count() >= minGhostDeactivationCount) {
				if (IsActive) {
					SetActive(false);
					isActive = false;
					IsActive = false;
					AudioNodesManager.PostEvent("DeactivateShelter", gameObject);
				}
			}


			yield return new WaitForSeconds(1f);
		}
	}

	public Transform player;
	public float playerActivationRange;

	bool createdEnemyForThisShelter = false;
	public Transform enemyPrefab;
	public float spawnEnemyInRadius = 190f;
	public float spawnEnemyHeight = 10f;

	IEnumerator CheckForPlayer()
	{
		yield return new WaitForSeconds(Random.Range(0, 1f));
		while (true) {
			var playerNearby = (player.position - transform.position).sqrMagnitude < playerActivationRange * playerActivationRange;
			if (playerNearby) {
				if (!IsActive) {
					SetActive(true);
					isActive = true;
					IsActive = true;

					if (GameObject.FindObjectsOfType<ShelterActivation>().All(sa => sa.IsActive)) {
						WinConditions.instance.Win();
					}

					// create enemy for this shelter.
					if (!createdEnemyForThisShelter) {
						var randomAngle = Random.Range(0, 2*Mathf.PI);
						var randomOnCircle = new Vector3(Mathf.Sin(randomAngle), 0, Mathf.Cos(randomAngle)) * spawnEnemyInRadius;
						var enemy = Instantiate(enemyPrefab, randomOnCircle + transform.position + Vector3.up * spawnEnemyHeight, Quaternion.identity) as Transform;
						GetComponentInChildren<MapSphere>().ResetMap();
						createdEnemyForThisShelter = true;
					}
				}
			}

			yield return new WaitForSeconds(0.5f);
		}
	}

}
