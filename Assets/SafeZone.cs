﻿using UnityEngine;
using System.Collections;

public class SafeZone : MonoBehaviour {

	public bool In, Out;

	void OnTriggerEnter(Collider c)
	{
		if (In) {
			if (c.GetComponent<PlayerSafe>()) {
				c.GetComponent<PlayerSafe>().safe = true;
				c.GetComponent<PlayerSafe>().safeZone = this;
				c.GetComponent<WobbleEffect>().Wobble();

				AudioNodesManager.PostEvent("EnterShelter", gameObject);
			}

		}
	}

	void OnTriggerExit(Collider c)
	{
		if (Out) {
			if (c.GetComponent<PlayerSafe>()) {
				c.GetComponent<PlayerSafe>().safe = false;
				c.GetComponent<PlayerSafe>().safeZone = null;
				c.GetComponent<WobbleEffect>().Wobble();

				AudioNodesManager.PostEvent("ExitShelter", gameObject);
			}
		}
	}
}
