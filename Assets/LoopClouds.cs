﻿using UnityEngine;
using System.Collections;

public class LoopClouds : MonoBehaviour
{

	public float loopMinX = -1500, loopMaxX = 1500;

	void Start()
	{

	}

	void Update()
	{
		if (transform.position.x > loopMaxX) {
			transform.position += Vector3.right * (loopMinX - loopMaxX);
		}
	}

	void OnDrawGizmos()
	{
		Gizmos.color = Color.cyan;
		Gizmos.DrawLine(Vector3.right * loopMinX + Vector3.up * 200 - Vector3.forward * 100, Vector3.right * loopMinX + Vector3.up * 200 + Vector3.forward * 100);
		Gizmos.DrawLine(Vector3.right * loopMaxX + Vector3.up * 200 - Vector3.forward * 100, Vector3.right * loopMaxX + Vector3.up * 200 + Vector3.forward * 100);

	}
}
