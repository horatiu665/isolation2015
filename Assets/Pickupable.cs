﻿using UnityEngine;
using System.Collections;

public class Pickupable : MonoBehaviour
{

	public enum Pickups
	{
		Construction,
		Potato
	}

	public Pickups type = Pickups.Construction;

	void OnTriggerEnter(Collider c)
	{
		if (c.GetComponent<Inventory>()) {
			switch (type) {
			case Pickups.Construction:
				c.GetComponent<Inventory>().amountConstruction++;
				break;
			case Pickups.Potato:
				PotatoCounter.potatoAmount += 1f;
				FuckCameraWhenNotSafe.instance.StartCoroutine(pTween.To(FuckCameraWhenNotSafe.instance.potatoPickupSafetyAnimDuration, t => {
					FuckCameraWhenNotSafe.instance.unsafeTime -= FuckCameraWhenNotSafe.instance.potatoPickupSafetyAmount * Time.deltaTime / FuckCameraWhenNotSafe.instance.potatoPickupSafetyAnimDuration;
				}));
				AudioNodesManager.PostEvent("PickupPotato", c.gameObject);
				break;
			default:
				break;
			}

			Destroy(transform.parent.gameObject);
		}
	}

}
