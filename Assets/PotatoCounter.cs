﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class PotatoCounter : MonoBehaviour
{

	public List<Transform> potatos = new List<Transform>();
	public static float potatoAmount = 20;
	public float potatoDepleteSpeed = 1f;

	void Start()
	{
		for (int i = 0; i < transform.childCount; i++) {
			potatos.Add(transform.GetChild(i));
		}
	}

	void Update()
	{
		// IsActive as many potatos from the list as we have amount of potatos
		// scale up remainder of <1 potato
		int i = 0;

		int maxPotato = Mathf.FloorToInt(potatoAmount - 1f);
		float lastPotato = (potatoAmount - (int)potatoAmount);
		for (i = 0; i < potatos.Count; i++) {
			if (i <= maxPotato) {
				potatos[i].localScale = Vector3.one;
			} else if (i == maxPotato + 1) {
				potatos[i].localScale = lastPotato * Vector3.one;
			} else {
				potatos[i].localScale = Vector3.zero;
			}
		}
	}
}
