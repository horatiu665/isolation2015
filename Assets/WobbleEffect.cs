﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class WobbleEffect : MonoBehaviour
{

	public Camera[] cams;

	public float duration;
	public float intensity;
	public AnimationCurve curve;

	// Use this for initialization
	public void Wobble()
	{
		StartCoroutine(pTween.To(duration, t => {
			foreach (var c in cams) {

				c.GetComponent<Fisheye>().strengthX = curve.Evaluate(t) * intensity;
				c.GetComponent<Fisheye>().strengthY = curve.Evaluate(t) * intensity;
			}
		}));
	}
}
