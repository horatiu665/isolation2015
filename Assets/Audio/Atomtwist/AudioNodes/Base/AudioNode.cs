﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
using Kaae;
#if UNITY_EDITOR
using UnityEditor;
#endif

public abstract class AudioNode : Node {

	//mixer routing
	[System.Serializable]
	public struct MixerGroupProperties
	{
		public AudioMixerGroup mixerGroup;
		public bool overrideParentMixerGroup;
	}

#if UNITY_EDITOR
	[CustomPropertyDrawer(typeof(MixerGroupProperties))]
	public class MixerGroupPropertiesDrawer : PropertyDrawer
	{
		public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUILayout.PropertyField(property.FindPropertyRelative("mixerGroup"));
			EditorGUILayout.PropertyField(property.FindPropertyRelative("overrideParentMixerGroup"));
		}
	}
#endif

	[HideInInspector]
	public int originAudioNodeID;
	[HideInInspector]
	public bool isPlaying = false;
	public MixerGroupProperties mixerGroupProperties;

	public override void OnValidate ()
	{
		base.OnValidate ();
		var audioNodes = GetComponentsInChildren<AudioNode>();
		foreach (var n in audioNodes)
		{
			if (!n.mixerGroupProperties.overrideParentMixerGroup)
				n.mixerGroupProperties.mixerGroup = mixerGroupProperties.mixerGroup;
		}
	}

	//TODO: Auditioning needs to be its owen Inteface & Push 2D Settings when triggered from Editor
	[DebugButton]
	public void Audition()
	{
		var p = this as IPlayable;
		p.Play(1, 1, 0);
	}

	//playback related
	[Range(0,1)]
	public float nodevolume = 1;
	[Range(0,2)]
	public float nodePitch = 1;
	[Range(0,2)]
	public float nodeDelay;

}
