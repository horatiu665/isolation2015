﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Linq;
using Kaae;
using UnityEngine.EventSystems;


public class EventNode : Node {

	[SerializeField]
	[HideInInspector]
	public List<EventAction> eventAction;
		
	public void PostEvent(int uniqueEventID)
	{
		if (uniqueID != uniqueEventID) return;
		foreach (var e in eventAction)
		{
			e.ExecuteEventAction(FindObjectOfType<AudioNodesManager>().gameObject);
		}
	}
	

	public void PostEvent(string eventName)
	{
		if (eventName != name) return;
		foreach (var e in eventAction)
		{
			e.ExecuteEventAction(FindObjectOfType<AudioNodesManager>().gameObject);
		}
	}
	
	public void PostEvent(int uniqueEventID, GameObject targetGameObject)
	{
		if (uniqueID != uniqueEventID) return;
		foreach (var e in eventAction)
		{
			e.ExecuteEventAction(targetGameObject);
		}
	}
	

	public void PostEvent(string eventName, GameObject targetGameObject)
	{
		if (eventName != name) return;
		foreach (var e in eventAction)
		{
			e.ExecuteEventAction(targetGameObject);
		}
	}

	[DebugButton]
	public void AuditionEvent()
	{
		foreach (var e in eventAction)
		{
			e.ExecuteEventAction(FindObjectOfType<AudioNodesManager>().gameObject);
		}
	}

	[DebugButton]
	public void StopAudition()
	{
		var audioNodePlayers = FindObjectsOfType<AudioNode>().Cast<IPlayable>();
		foreach (var p in audioNodePlayers)
		{
			p.Stop();
		}
	}




}
