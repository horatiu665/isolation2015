﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
#endif
using UnityEngine.Audio;


public enum EventActionType
{
	Play, 
	Stop,
	StopAll,
	TriggerMixerSnapshot,
}

public enum EventActionScope
{
	Gameobject,
	Global,
}

[System.Serializable]
public class EventAction
{
	public int uniqueAudioNodeID;
	public EventActionType actionType;
	public GameObject selectedNodeGameObject;
	public EventActionScope actionScope;
	public AudioMixerSnapshot mixerSnapshot;
	public float mixerSnapshotTransitionTime;
	
	//TODO: Need an applySettings Method here to apply settings as eventactions before play
	
	public void ExecuteEventAction(GameObject targetGameObject)
	{
		GameObject nodeObject;
		switch (actionType)
		{
		case EventActionType.Play : 
			//create instance of audioEvent
			nodeObject = GameObject.Instantiate<GameObject>(selectedNodeGameObject);
			//TODO: make eventInterface to update the settings
			nodeObject.GetComponent<AudioNode>().originAudioNodeID = uniqueAudioNodeID;
			nodeObject.AddComponent<DestroyAfterPlay>();
			nodeObject.transform.SetParent(targetGameObject.transform);
			nodeObject.transform.localPosition = Vector3.zero;
			ExecuteEvents.Execute<IPlayable>(nodeObject,null, (x,y) => x.Play(1, 1, 0) );
			break;
		case EventActionType.Stop  :
			if (actionScope == EventActionScope.Gameobject)
			{
				var nodeObjects = targetGameObject.GetComponentsInChildren<AudioNode>();
				foreach (var n in nodeObjects)
				{
					if (n.originAudioNodeID == uniqueAudioNodeID)
					{
						ExecuteEvents.Execute<IPlayable>(n.gameObject,null, (x,y) => x.Stop() );
					}
					
				}
			}
			if (actionScope == EventActionScope.Global)
			{
				var nodeObjects = GameObject.FindObjectsOfType<AudioNode>();
				foreach (var n in nodeObjects)
				{
					if (n.originAudioNodeID == uniqueAudioNodeID)
						ExecuteEvents.Execute<IPlayable>(n.gameObject,null, (x,y) => x.Stop() );
				}
			}
			break;
		case EventActionType.StopAll  :
			if (actionScope == EventActionScope.Gameobject)
			{
				var nodeObjects = targetGameObject.GetComponentsInChildren<AudioNode>();
				foreach (var n in nodeObjects)
				{
					ExecuteEvents.Execute<IPlayable>(n.gameObject,null, (x,y) => x.Stop() );
				}
			}
			if (actionScope == EventActionScope.Global)
			{
				var nodeObjects = GameObject.FindObjectsOfType<AudioNode>();
				foreach (var n in nodeObjects)
				{
					ExecuteEvents.Execute<IPlayable>(n.gameObject,null, (x,y) => x.Stop() );
				}
			}
			break;
		case EventActionType.TriggerMixerSnapshot :
			mixerSnapshot.TransitionTo(mixerSnapshotTransitionTime);

			break;
		}
	}
}



