﻿using UnityEngine;
using System.Collections;

/// <summary>
/// when something collides with this collider, it respawns above ground or in its original location (if possible)
/// </summary>
public class SafetyNetDeathCollider : MonoBehaviour
{

	void OnTriggerEnter(Collider c)
	{
		if (c.tag == "Player" || c.GetComponent<PlayerSafe>()) {
			// kill player and reset level or something
			RestartScene.RestartLevel();
		} else if (c.GetComponentInParent<RestartableObject>()) {
			c.GetComponentInParent<RestartableObject>().RestartObject();
		}
	}
}
